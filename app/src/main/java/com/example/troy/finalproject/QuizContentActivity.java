package com.example.troy.finalproject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.StateListDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Debug;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NotificationCompatSideChannelService;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Collections;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class QuizContentActivity extends BaseMenuActivity {



    private ProgressDialog pDialog;



    private EditText mContent;
    private Object jContent;
    private String catagory;
    private String coranswer1, answer1, answer2, answer3, answer4, question, questionid, correctQuestions, completedQuestionsc;
    private String ran1, ran2, ran3, ran4, completedQuestions, ansCheck;
    private int completedQuestionsi, correctQuestionsi;


    // URL to get questions JSON
    private static String url = "http://10.0.3.2/project_middleware/quiz.php";

    // JSON Node names
    private static final String TAG_QUIZ = "quiz";
    private static final String TAG_CORANSWER1 = "coranswer1";
    private static final String TAG_ANSWER2 = "answer2";
    private static final String TAG_ANSWER3 = "answer3";
    private static final String TAG_ANSWER4 = "answer4";
    private static final String TAG_QUESTION = "question";
    private static final String TAG_QUESTIONID = "id";


    // quiz JSONArray
    JSONArray quiz = null;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("Debug", "Started class");
        setContentView(R.layout.activity_quiz_content);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        Intent intent = getIntent();

        catagory = intent.getStringExtra("catagory"); //obtains the catagory
        completedQuestionsc = intent.getStringExtra("completedQuestionsc"); //obtains the completed questions count
        completedQuestions = intent.getStringExtra("completedQuestions"); //obtains the id's of the completed questions
        correctQuestions = intent.getStringExtra("correctQuestions"); //obtains correctly answered questions count

        //keeps count of how many questions there have been.
        completedQuestionsi = (Integer.parseInt(completedQuestionsc)+1);

        Log.d("begin: ", "> " + correctQuestions + completedQuestionsc);


        Log.e("Debug", "Reached pre-server conntection");
        new GetContent().execute();
        Log.e("Debug", "reched post-server connections");

        setSupportActionBar(toolbar);



        Button a1 = (Button) findViewById(R.id.btnA1);


        a1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                ((Button)findViewById(R.id.btnConfirm)).setText(ran1 + " confirm?");


            }
        });

        Button a2 = (Button) findViewById(R.id.btnA2);


        a2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                ((Button)findViewById(R.id.btnConfirm)).setText(ran2 + " confirm?");


            }
        });

        Button a3 = (Button) findViewById(R.id.btnA3);


        a3.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                ((Button)findViewById(R.id.btnConfirm)).setText(ran3 + " confirm?");

            }
        });

        Button a4 = (Button) findViewById(R.id.btnA4);

        a4.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                ((Button)findViewById(R.id.btnConfirm)).setText(ran4 + " confirm?");

            }
        });

        Button back = (Button) findViewById(R.id.btnBackQc);

        back.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), QuizMenuActivity.class);

                startActivity(intent);


            }
        });


    final Button btnConfirm = (Button) findViewById(R.id.btnConfirm);

    btnConfirm.setOnClickListener(new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            btnConfirm.setEnabled(false);

            Intent intent;


            //checks if the question limit is reached so the user can be passed to the results summery screen
            if(completedQuestionsi > 9)
            {
                intent = new Intent(v.getContext(), QuizResultActivity.class);
            }
            else
            {
                intent = new Intent(v.getContext(), QuizContentActivity.class);
            }

            ansCheck = (coranswer1 + " confirm?");

            //places the question's id into a list so it will not come back

            intent.putExtra("completedQuestions", completedQuestions + "," + questionid);


            //checks if the answer is correct.
            if (ansCheck.equals(btnConfirm.getText()))
            {
                correctQuestionsi = (Integer.parseInt(correctQuestions) + 1);
            }
            else
            {
                correctQuestionsi = (Integer.parseInt(correctQuestions));
            }


            Log.d("end: ", "> " + correctQuestionsi + completedQuestionsi);

            intent.putExtra("completedQuestionsc", Integer.toString(completedQuestionsi));
            intent.putExtra("correctQuestions", Integer.toString(correctQuestionsi));

            intent.putExtra("catagory", catagory);


                startActivity(intent);




        }
    });

        Log.e("Debug", "End class");
}



    private class GetContent extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(QuizContentActivity.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected Void doInBackground(Void... params) {




            // Adding as parameters
            ArrayList<NameValuePair> param = new ArrayList<NameValuePair>();

            param.add(new BasicNameValuePair("catagory", catagory));
            param.add(new BasicNameValuePair("qids", completedQuestions));


            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            // Making a request to url and getting response
            String jsonStrE = ("  {\"quiz\":[{\"id\":\"21\",\"catagory\":\"s\",\"question\":\"There has been a problems with the server. Please check your internet or try again. If that doesn not work then it may be our servers that are down.\",\"coranswer1\":\"...\",\"answer2\":\" ...\",\"answer3\":\"...\",\"answer4\":\"...\",\"hint\":\"Better luck next time\"}]} ");
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.GET, param);

            if (jsonStr == null)
            {
                jsonStr = jsonStrE;
            }

            if (jsonStr == "")
            {
            jsonStr = sh.makeServiceCall(url, ServiceHandler.GET, param);

                if (jsonStr == "")
                {
                    jsonStr = jsonStrE;
                }
            }



            Log.d("Response: ", "> " + jsonStr);

            if (jsonStr != null) {
                try {
                    JSONObject jObj = new JSONObject(jsonStr);

                    // Getting JSON Array node
                    quiz = jObj.getJSONArray(TAG_QUIZ);

                    // looping through all questions
                    for (int i = 0; i < quiz.length(); i++) {
                        JSONObject c = quiz.getJSONObject(i);
                        questionid = c.getString(TAG_QUESTIONID);
                        question = c.getString(TAG_QUESTION);
                        coranswer1 = c.getString(TAG_CORANSWER1);
                        answer2 = c.getString(TAG_ANSWER2);
                        answer3 = c.getString(TAG_ANSWER3);
                        answer4 = c.getString(TAG_ANSWER4);

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");


            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into Content page
             */

            //This creates an array of the questions and then randomises it. That allows the random received questions result
            //to get put into the buttons at random
            String[] randomArray = {coranswer1,answer2,answer3,answer4};

            Log.d("randomArray: " , randomArray[1]);

            String[] nrandomArray = Randomize(randomArray);

            Log.d("randomArray: " , nrandomArray[1] );

            ran1 = nrandomArray[0];
            ran2 = nrandomArray[1];
            ran3 = nrandomArray[2];
            ran4 = nrandomArray[3];

            Log.d("random1: " , ran1 );

            //assigns the values to the quiz screen
            ((Button)findViewById(R.id.btnA1)).setText(ran1);
            ((Button)findViewById(R.id.btnA2)).setText(ran2);
            ((Button)findViewById(R.id.btnA3)).setText(ran3);
            ((Button)findViewById(R.id.btnA4)).setText(ran4);
            ((TextView)findViewById(R.id.tvQuestion)).setText(completedQuestionsi + ". " + question);



        }


    }

    //randomises an array that gets put into it
    public static String[] Randomize(String[] arr) {
        String[] randomizedArray = new String[arr.length];
        System.arraycopy(arr, 0, randomizedArray, 0, arr.length);
        Random rgen = new Random();

        for (int i = 0; i < randomizedArray.length; i++) {
            int randPos = rgen.nextInt(randomizedArray.length);
            String tmp = randomizedArray[i];
            randomizedArray[i] = randomizedArray[randPos];
            randomizedArray[randPos] = tmp;
        }

        return randomizedArray;
    }


}