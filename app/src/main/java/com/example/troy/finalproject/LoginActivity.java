package com.example.troy.finalproject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class LoginActivity extends BaseMenuActivity {

    private ProgressDialog pDialog;
    private String success = "false";


    private String pass,email;

    // URL used to gains account details
    private static String url = "http://10.0.3.2/project_middleware/login.php";

    // JSON Node names
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_USER_ID = "id";
    private static final String TAG_FIRST_NAME = "forename";
    private static final String TAG_LAST_NAME = "surname";
    private static final String TAG_EMAIL = "email";
    private static final String TAG_LOGIN = "accountDetails";

    JSONArray accountDetails = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // adding listener to register click
        Button register = (Button) findViewById(R.id.btnRegister);


        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), RegisterActivity.class);
                startActivityForResult(intent, 0);


            }
        });

        // adding listener to register click
        Button login = (Button) findViewById(R.id.btnLogin);


        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                email = ((EditText) findViewById(R.id.etEmail)).getText().toString();
                pass = ((EditText) findViewById(R.id.etPassword)).getText().toString();

                new Login().execute();




            }
        });

        Button back = (Button) findViewById(R.id.btnBackLo);


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), MainActivity.class);

                startActivity(intent);

            }
        });



        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private class Login extends AsyncTask<Void, Void, Void> {


        Boolean loggedIn = false;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(LoginActivity.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected Void doInBackground(Void... params) {


            // Adding as parameters
            ArrayList<NameValuePair> param = new ArrayList<NameValuePair>();

            param.add(new BasicNameValuePair("email", email));
            param.add(new BasicNameValuePair("password", pass));


            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.GET, param);

            Log.d("Response: ", "> " + jsonStr);

            if (jsonStr != null) {

                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);

                    // Getting JSON Array node
                    accountDetails = jsonObj.getJSONArray(TAG_LOGIN);

                    JSONObject c = accountDetails.getJSONObject(0);

                    if(jsonObj != null)
                    {
                        success = "true";
                    }

                    String userID = c.getString(TAG_USER_ID);
                    String forename = c.getString(TAG_FIRST_NAME);
                    String surname = c.getString(TAG_LAST_NAME);
                    String email = c.getString(TAG_EMAIL);

                    // If a successful login
                    if (success.equals("true")) {
                        // Add logged in user's shared preferences
                        SharedPreferences pref = getApplicationContext().getSharedPreferences("Preferences", 0);
                        SharedPreferences.Editor editor = pref.edit();

                        editor.putString("user_id", userID);
                        editor.putString("forename", forename);
                        editor.putString("surname", surname);
                        editor.putString("email", email);
                        editor.putBoolean("loggedIn", true);


                        editor.commit();

                        Log.e("code", "reached this point");

                        loggedIn = true;


                    }

                    else {
                        loggedIn = false;
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }

            return null;
        }


        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            // error toast
            if (loggedIn == false) {
                pDialog.hide();
                Toast toast = Toast.makeText(getApplicationContext(), "Invalid login", Toast.LENGTH_SHORT);
                toast.show();
            }

            // confirmation toast
            else {
                Toast toast = Toast.makeText(getApplicationContext(), "Login successful", Toast.LENGTH_SHORT);
                toast.show();
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivityForResult(intent, 0);
            }
        }

    }

}
