package com.example.troy.finalproject;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class LearnContentActivity extends BaseMenuActivity {

    private ProgressDialog pDialog;


    private EditText mContent;
    private Object jContent;
    private String contents;
    private String catagory, page, catapage;
    int pagep;
    int pagem;






    // URL to get Events JSON
    private static String url = "http://10.0.3.2/project_middleware/";

    // JSON Node names
    private static final String TAG_CONTENT = "content";

    private static final String TAG_CATAGORYPAGE = "catagorypage";
    private static final String TAG_CONTENTS = "contents";


    // Events JSONArray
    JSONArray content = null;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_learn_content);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        final Button next = (Button) findViewById(R.id.btnNextLc);
        final Button quiz = (Button) findViewById(R.id.btnQuizLc);



        GetDetails();
        Log.e("code", "reched pre-class run");
        new GetContent().execute();
        Log.e("code", "reched post-class-run");

        //button for next page. It will add one to the page count and load that from the database.




        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (contents != null) {

                    Intent intent = new Intent(v.getContext(), LearnContentActivity.class);

                    intent.putExtra("page", Integer.toString(pagep));
                    intent.putExtra("catagory", catagory);

                    startActivity(intent);
                } else {
                    next.setEnabled(false);
                    next.setVisibility(v.GONE);
                    quiz.setVisibility(v.VISIBLE);

                }

            }


        });

        quiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                    Intent intent = new Intent(v.getContext(), QuizContentActivity.class);

                    intent.putExtra("catagory", catagory);
                    intent.putExtra("completedQuestions", "0");
                    intent.putExtra("correctQuestions", "0");
                    intent.putExtra("completedQuestionsc", "0");

                    startActivity(intent);


            }


        });

        //button to go back a page, if the page number is one then it will return to the menu.
        Button back = (Button) findViewById(R.id.btnBackLc);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (pagem == 0)
                {
                    Intent intent = new Intent(v.getContext(), LearnMenuActivity.class);
                    startActivityForResult(intent, 0);
                }else{
                    Intent intent = new Intent(v.getContext(), LearnContentActivity.class);

                    intent.putExtra("page", Integer.toString(pagem));
                    intent.putExtra("catagory", catagory);

                    startActivity(intent);
                }


            }


        });

        //button to go back a page, if the page number is one then it will return to the menu.
        Button menu = (Button) findViewById(R.id.btnMenuLc);

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(v.getContext(), LearnMenuActivity.class);
                startActivityForResult(intent, 0);


            }


        });

        TextView title = (TextView) findViewById(R.id.tvTitle);

        String catagoryString = "hi";
        String temp = catagory;
        switch (temp) {
            case "c":
                catagoryString = "Copyright";
                break;
            case "s":
                catagoryString = "Social Media";
                break;
            case "i":
                catagoryString = "Internet Saftey";
                break;

        }
            title.setText(page + ". " + catagoryString);


    }


    public void GetDetails() {


        Intent intent = getIntent();

         page = intent.getStringExtra("page");
         pagep = (Integer.parseInt(page)+1);
         pagem = (Integer.parseInt(page)-1);
         catagory = intent.getStringExtra("catagory");
         catapage = catagory+ page;

        Log.d("Values: ", "> " + "page: " + page + "pagep: " + pagep + "pagem: " + pagem + "catagory: " + catagory + "catapagepage: " + catapage);



    }

    /*    private class GetContent extends AsyncTask<String, String, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(LearnContentActivity.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

            @Override
            protected JSONObject doInBackground(String... args) {


                // Creating service handler class instance
                JSONParser jp = new JSONParser();


                try {

                    HashMap<String, String> params = new HashMap<>();
                    params.put("page", page);
                    params.put("catagory", catagory);

                    Log.d("request", "starting");

                    JSONObject json = jp.makeHttpRequest(
                            url, "GET", params);

                    if (json != null) {
                        Log.d("JSON result", json.toString());

                        return json;
                    }


                    Log.d("Response: ", "> " + json);

                    if (json != null) {


                        // Getting JSON Array node
                        content = json.getJSONArray(TAG_CONTENT);

                        // looping through All Events
                        for (int i = 0; i < content.length(); i++) {
                            JSONObject c = content.getJSONObject(i);
                            contents = c.getString(TAG_CONTENTS);
                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


                return null;
            }

            protected void onPostExecute(JSONObject json) {




                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
            /**
             * Updating parsed JSON data into Content page
             */


  //  ((EditText)findViewById(R.id.etLearn)).setText(contents);






    private class GetContent extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(LearnContentActivity.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected Void doInBackground(Void... params) {


            // Adding as parameters
            ArrayList<NameValuePair> param = new ArrayList<NameValuePair>();

            param.add(new BasicNameValuePair("catagorypage", catapage));





            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.GET, param);

            Log.d("Response: ", "> " + jsonStr);

            if (jsonStr != null) {
                try {
                    JSONObject jObj = new JSONObject(jsonStr);

                    // Getting JSON Array node
                    content = jObj.getJSONArray(TAG_CONTENT);

                    // looping through All Events
                    for (int i = 0; i < content.length(); i++) {
                        JSONObject c = content.getJSONObject(i);
                        contents = c.getString(TAG_CONTENTS);
                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into Content page
             */

            EditText etLearn = ((EditText) findViewById(R.id.etLearn));

            if(contents != null) {

                etLearn.setText(contents);

            }
            else
            {
                etLearn.setText("end of learning!");
            }




//            if (etLearn.getText() = null)
//            {
//                etLearn.setText("end of contents");
//            }


        }

    }

}
