package com.example.troy.finalproject;

/**
 * Created by Troy on 14/03/2017.
 */
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

public abstract class BaseMenuActivity extends AppCompatActivity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        setTitle("Child Internet Safeties");
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.home:
                Intent i0 = new Intent(this, MainActivity.class);
                this.startActivity(i0);
                return true;
            case R.id.learnMenu:
                Intent i1 = new Intent(this, LearnMenuActivity.class);
                this.startActivity(i1);
                return true;
            case R.id.quizMenu:
                Intent i2 = new Intent(this,QuizMenuActivity.class);
                this.startActivity(i2);
                return true;
           // case R.id.stats:
           //     Intent i3 = new Intent(this,OrderHistoryActivity.class);
           //     this.startActivity(i3);
           //     return true;
            case R.id.about:
                Intent i4 = new Intent(this,AboutActivity.class);
                this.startActivity(i4);
                return true;
           // case R.id.settings:
           //    Intent i5 = new Intent(this,SettingsActivity.class);
           //     this.startActivity(i5);
           //     return true;

           // case R.id.action_settings:
           //     return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
