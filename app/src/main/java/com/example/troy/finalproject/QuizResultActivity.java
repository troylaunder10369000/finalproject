package com.example.troy.finalproject;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class QuizResultActivity extends AppCompatActivity {

    private String score, completeScore, feedback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_result);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();

        score = intent.getStringExtra("correctQuestions");
        completeScore = intent.getStringExtra("completedQuestionsc");

        ((TextView)findViewById(R.id.tvScore)).setText(score);
        ((TextView)findViewById(R.id.tvCompleteScore)).setText(completeScore);

        int temp = Integer.parseInt(score);
        switch (temp) {
            case 0:
                ((TextView)findViewById(R.id.tvFeedback)).setText("You didn't get any correct, It can only get better from here. Try reviewing the learning content before attemtping again.");
                break;
            case 1:
                ((TextView)findViewById(R.id.tvFeedback)).setText("Perhaps you should try going over the learning material before trying the quiz again.");
                break;
            case 2:
                ((TextView)findViewById(R.id.tvFeedback)).setText("Try going over the content again before retrying the quiz");
                break;
            case 3:
                ((TextView)findViewById(R.id.tvFeedback)).setText("A bit below half marks. Maybe check the content again before trying the quiz once more.");
                break;
            case 4:
                ((TextView)findViewById(R.id.tvFeedback)).setText("Slightly below half marks. Maybe check the content again before trying the quiz once more.");
                break;
            case 5:
                ((TextView)findViewById(R.id.tvFeedback)).setText("Half way there. Go and check out the content or try again if you felt it was just silly mistakes.");
                break;
            case 6:
                ((TextView)findViewById(R.id.tvFeedback)).setText("You got more right than you got wrong. Try again or review the content.");
                break;
            case 7:
                ((TextView)findViewById(R.id.tvFeedback)).setText("You have a good grasp on this quiz. Try again for higher scores");
                break;
            case 8:
                ((TextView)findViewById(R.id.tvFeedback)).setText("Very close! You can do it.");
                break;
            case 9:
                ((TextView)findViewById(R.id.tvFeedback)).setText("Almost! Try again and give it your best.");
                break;
            case 10:
                ((TextView)findViewById(R.id.tvFeedback)).setText("A perfect score. Come back in a few weeks to make sure you keep it this way!");
                break;


        }


        Button menu = (Button) findViewById(R.id.btnMenuQr);
        menu.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent intent = new Intent(v.getContext(), MainActivity.class);

                startActivity(intent);


            }
        });

        Button learn = (Button) findViewById(R.id.btnLearnQr);
        learn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent intent = new Intent(v.getContext(), LearnMenuActivity.class);

                startActivity(intent);


            }
        });

        Button quiz = (Button) findViewById(R.id.btnQuizQr);
        quiz.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent intent = new Intent(v.getContext(), QuizMenuActivity.class);

                startActivity(intent);


            }
        });
            }

    }


