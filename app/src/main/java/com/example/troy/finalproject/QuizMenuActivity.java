package com.example.troy.finalproject;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

public class QuizMenuActivity extends BaseMenuActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_menu);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        Button internetSaftey = (Button) findViewById(R.id.btnInternetSaftey);


        internetSaftey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), QuizContentActivity.class);


                intent.putExtra("catagory", "i");
                intent.putExtra("correctQuestions", "0");
                intent.putExtra("completedQuestionsc", "0");

                startActivity(intent);

            }
        });
        Button copyright = (Button) findViewById(R.id.btnCopyright);


        copyright.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), QuizContentActivity.class);


                intent.putExtra("catagory", "c");
                intent.putExtra("completedQuestions", "0");
                intent.putExtra("correctQuestions", "0");
                intent.putExtra("completedQuestionsc", "0");

                startActivity(intent);

            }
        });
        Button socialMedia = (Button) findViewById(R.id.btnSocialMedia);


        socialMedia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), QuizContentActivity.class);



                intent.putExtra("catagory", "s");
                intent.putExtra("completedQuestions", "0");
                intent.putExtra("correctQuestions", "0");
                intent.putExtra("completedQuestionsc", "0");

                startActivity(intent);

            }
        });

        Button back = (Button) findViewById(R.id.btnBackQm);


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), MainActivity.class);

                startActivity(intent);

            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

}
