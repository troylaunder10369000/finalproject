package com.example.troy.finalproject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class RegisterActivity extends BaseMenuActivity{

    private ProgressDialog pDialog;
    private String email,forename,surname,password,confirmEmail,confirmPassword,reply = "fail";
    private String error1 = "Error, please fill in everything perfectly.";
    TextView tvError;

    // URL to get Register JSON
    private static String url = "http://10.0.3.2/project_middleware/register.php";

    private static final String TAG_REGISTER = "register";
    private static final String TAG_SUCCESS = "success";


    // register JSONArray
    JSONArray register = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Button btnSubmit = (Button) findViewById(R.id.btnSubmit);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  Intent intent = new Intent(v.getContext(), Register2Activity.class);

                // Checking email and confirmed email match and getting data to send
                forename = ((EditText) findViewById(R.id.etEmail)).getText().toString();
                surname = ((EditText) findViewById(R.id.etEmail)).getText().toString();
                email = ((EditText) findViewById(R.id.etEmail)).getText().toString();
                confirmEmail = ((EditText) findViewById(R.id.etEmailConfirm)).getText().toString();
                password = ((EditText) findViewById(R.id.etPassword)).getText().toString();
                confirmPassword = ((EditText) findViewById(R.id.etPasswordConfirm)).getText().toString();

                if (email.equals(confirmEmail) && password.equals(confirmPassword)) {

                    new sendRegister().execute();


                } else {

                    ((TextView) findViewById(R.id.tvError)).setText("Email or Password are not the same, Please re-check and try again");
                    (findViewById(R.id.tvError)).setVisibility(View.VISIBLE);

                }

            }
        });


    }

    private class sendRegister extends AsyncTask<Void, Void, Void> {

        boolean registered = false;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(RegisterActivity.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected Void doInBackground(Void... params) {


            // Checking password and confirmed password match
            // Adding as parameters
            ArrayList<NameValuePair> param = new ArrayList<NameValuePair>();

            param.add(new BasicNameValuePair("forename", forename));
            param.add(new BasicNameValuePair("surname", surname));
            param.add(new BasicNameValuePair("email", email));
            param.add(new BasicNameValuePair("password", password));

            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            Log.d("URL: ", "> " + sh);

            //sending the parameters
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.GET, param);


            Log.d("Response: ", "> " + jsonStr);

            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);

                    // Getting JSON Array node
                    register = jsonObj.getJSONArray(TAG_REGISTER);

                    JSONObject c = register.getJSONObject(0);

                    String success = c.getString(TAG_SUCCESS);

                    // If a successful register
                    if (success.equals("true")) {




                        Log.e("code", "reached this point");

                        registered = true;


                    }

                    else {
                        registered = false;
                    }




                    }catch(JSONException e){
                        e.printStackTrace();
                    }
                }else{
                    Log.e("ServiceHandler", "Couldn't get any data from the url");


                }

                return null;
            }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into Content page
             */

            // error toast
            if (registered = false) {
                //tiered errors


                    ((TextView) findViewById(R.id.tvError)).setText("Unknown error");
                    (findViewById(R.id.tvError)).setVisibility(View.VISIBLE);


            }

            // confirmation toast
            else {
                Toast toast = Toast.makeText(getApplicationContext(), "Registration successful", Toast.LENGTH_SHORT);
                toast.show();

                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivityForResult(intent, 0);
            }


            }
        }

    }


