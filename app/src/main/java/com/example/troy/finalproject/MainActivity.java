package com.example.troy.finalproject;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Button;

public class MainActivity extends BaseMenuActivity {

    Button learn;
    Boolean loggedIn;
    String email;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Reading user's current details from shared preferences
        SharedPreferences pref = getApplicationContext().getSharedPreferences("Preferences", 0);

        email = pref.getString("email", "null");
        loggedIn = pref.getBoolean("loggedIn", false);

        if (loggedIn != false)
        {
            ((Button)findViewById(R.id.btnLogin)).setText("Logout" + " " + email);
        }

        Log.d("Response: ", "> " + email + " " + loggedIn);

        // adding listener to register click
        Button learn = (Button) findViewById(R.id.btnLearn);


        learn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), LearnMenuActivity.class);
                startActivityForResult(intent, 0);

            }
        });

        // adding listener to register click
        Button quiz = (Button) findViewById(R.id.btnQuiz);


        quiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), QuizMenuActivity.class);
                startActivityForResult(intent, 0);

            }
        });

        // adding listener to register click
        Button login = (Button) findViewById(R.id.btnLogin);


        if(loggedIn != false)
        {

            pref = getApplicationContext().getSharedPreferences("Preferences", 0);
            pref.edit().clear().commit();


            login.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(v.getContext(), MainActivity.class);
                    startActivityForResult(intent, 0);

                }
            });
        }

        else {
            login.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(v.getContext(), LoginActivity.class);
                    startActivityForResult(intent, 0);

                }
            });
        }
        // adding listener to register click
        Button settings = (Button) findViewById(R.id.btnSettings);


        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), SettingsActivity.class);
                startActivityForResult(intent, 0);

            }
        });

        // adding listener to register click
        Button stats = (Button) findViewById(R.id.btnStats);


        stats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), StatsActivity.class);
                startActivityForResult(intent, 0);

            }
        });

        Button quit = (Button) findViewById(R.id.btnQuit);


        quit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
                System.exit(0);

            }
        });


    }
}
