package com.example.troy.finalproject;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

public class LearnMenuActivity extends BaseMenuActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_learn_menu);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        Button internetSaftey = (Button) findViewById(R.id.btnInternetSaftey);


        internetSaftey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), LearnContentActivity.class);

                intent.putExtra("page", "1");
                intent.putExtra("catagory", "i");

                startActivity(intent);

            }
        });
        Button copyright = (Button) findViewById(R.id.btnCopyright);


        copyright.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), LearnContentActivity.class);

                intent.putExtra("page", "1");
                intent.putExtra("catagory", "c");

                startActivity(intent);

            }
        });
        Button socialMedia = (Button) findViewById(R.id.btnSocialMedia);


        socialMedia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), LearnContentActivity.class);


                intent.putExtra("page", "1");
                intent.putExtra("catagory", "s");

                startActivity(intent);

            }
        });

        Button back = (Button) findViewById(R.id.btnBackLm);


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), MainActivity.class);

                startActivity(intent);

            }
        });




        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

}
